import { Component } from '@angular/core';

import {SelectItem} from 'primeng/api';

interface Estado {
  name: string;
  code: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nome: string;

  email: string;

  cep: string;

  endereco: string;

  numero: number;

  bairro: string;

  cidade: string;

  data: string;

  selectedSexo: string;
  
  estado: Estado[];

  selectedEstado: Estado;

  Cadastrado(){
    alert('Cadastrado com sucesso!');
  }

  constructor() {

    this.estado = [
        {name: 'Pernambuco', code: 'PE'},
        {name: 'Belo Horizonte', code: 'BH'},
        {name: 'Amazonas', code: 'AM'},
        {name: 'São Paulo', code: 'SP'},
        {name: 'Rio de Janeiro', code: 'RJ'}
    ];
}

}
